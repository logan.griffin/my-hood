My Hood - Created 10/09/2016, by Logan Griffin

This Application makes it possible to writing comments and upload photos of
your neighbours. All comments and photos that you post are stored locally on 
your iPhone and are not visible on the internet or on anyone elses devices.

This project was created while doing an iOS course apart of my learning. This
app was made alongside the iOS course "iOS 9 and Swift 2: From Beginner to 
Paid Professional" created by Mark Price. 

All graphics from this application were supplied from Mark Price and downloaded
for the udemy website.

My Hood applicaton was created for learning purposes, this application is not
intended for public use.